let filter = require('./filter.cjs');
const nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
function isEven(num, index, elements) {
    return (num % 2 === 0);
}
let evenValues = filter(nums, isEven);
console.log(evenValues);

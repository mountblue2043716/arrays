let each = require('./each.cjs');
const myArray = [2, 4, 6, 8, 10];
function cb(element, index) {
  console.log(`Element ${index}: ${element}`);
}
each(myArray, cb);

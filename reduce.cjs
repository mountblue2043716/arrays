function reduce(elements, cb, startingValue) {
    let result;
    let startIndex;
    if (startingValue !== undefined) {
      result = startingValue;
      startIndex = 0;
    } else {
      result = elements[0];
      startIndex = 1;
    }
    for (let index = startIndex; index < elements.length; index++) {
      result = cb(result, elements[index], index, elements);
    }
    return result;
  }
  module.exports = reduce;
  
flatten = require('./flatten.cjs')
const nestedArray = [1, [2], , [[3]], [[[4]]]]; // use this to test 'flatten'
const test2 = [1,2,3,6,4,5]
const test3 = [1,,,3,6,,2,3,20]
console.log(JSON.stringify(flatten(nestedArray, 2)));
console.log('test 2 output with user defined function')
console.log(flatten(test2));
console.log('test 2 output with inbuilt function')
console.log(test2.flat());
console.log('test 3 output with user defined function')
console.log(flatten(test3));
console.log('test 3 output with inbuilt function')
console.log(test3.flat());


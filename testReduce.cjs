let reduce = require('./reduce.cjs');
let numbers = [1, 2, 3, 4, 5];
function cb(accumulator, value) {
    return accumulator + value;
}
let sum = reduce(numbers, cb, 0);
console.log(sum); 

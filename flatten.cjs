
function flatten(elements, depth = 1) {
    let flattenedArray = []
    for (let index = 0; index < elements.length; index++) {
        if (depth <= 0) {
            return elements;
        }
        if (Array.isArray(elements[index])) {
            flattenedArray = flattenedArray.concat(flatten(elements[index], depth - 1));
        }
        else if (elements[index]) {
            flattenedArray.push(elements[index]);
        }
    }
    return flattenedArray;
}
module.exports = flatten;

let find = require('./find.cjs');
const nums = [0,1, 2, 3, 4, 5];
function isOdd(element) {
    return element%2 === 1;
}
let firstOddValue = find(nums, isOdd);
console.log(firstOddValue);

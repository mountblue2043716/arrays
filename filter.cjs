function filter(elements,cb) {
    let returnList = []
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            returnList.push(elements[index]);
        }
    }
    return returnList;
}

module.exports = filter;